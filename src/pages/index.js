import React from "react"
import { StaticImage } from "gatsby-plugin-image"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />

    <div style={{ maxWidth: `600px`, marginBottom: `1.45rem` }}>
      <StaticImage src="../images/banner.png" alt="hero" />
    </div>
    <div className="text-center bg-theme_white p-5 rounded-lg shadow-2xl shadow-theme_white">
      <h1 className="text-5xl text-center font-bold">
        {" "}
        Gatsby <span className="text-red-600">SuperFast</span> Starter 🦸‍♂️{" "}
      </h1>
      <p className="text-blue-500 mt-4 text-lg">
        {" "}
        author: <span className="text-black">"Sijin Raj"</span>
      </p>
    </div>
  </Layout>
)

export default IndexPage
